'use strict';

angular.module('main').service('eventsResolver', [
  'EventsCollection',
  function (
    EventsCollection
  ) {
    this.resolve = function () {
      EventsCollection.reset();

      return EventsCollection.fetch()
        .then(function (response) {
          console.log(response);
        });
    };
  }
]);