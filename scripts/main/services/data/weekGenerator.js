"use strict";

angular.module('main').service('weekGenerator', [
  function () {
    var months = [{
      number: 0,
      name: 'January',
      days: 31
    }, {
      number: 1,
      name: 'February',
      days: 28
    }, {
      number: 2,
      name: 'March',
      days: 31
    }, {
      number: 3,
      name: 'April',
      days: 30
    }, {
      number: 4,
      name: 'May',
      days: 31
    }, {
      number: 5,
      name: 'June',
      days: 30
    }, {
      number: 6,
      name: 'July',
      days: 31
    }, {
      number: 7,
      name: 'August',
      days: 31
    }, {
      number: 8,
      name: 'September',
      days: 30
    }, {
      number: 9,
      name: 'October',
      days: 31
    }, {
      number: 10,
      name: 'November',
      days: 30
    }, {
      number: 11,
      name: 'December',
      days: 31
    }];

    var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

    this.getCurrentMonthName = function () {
      var month = months[new Date().getMonth()];
      return month.name;
    };

    this.getCurrentDayName = function () {
      return days[new Date().getDay()];
    };

    this.getWeek = function () {
      return processWeek(new Date());
    };

    this.getMonth = function () {
      return processMonth(new Date());
    };

    function processMonth(temp) {
      var month = months[temp.getMonth()];
      var result = [];

      var leftIterator;
      for (leftIterator = temp.getDate() - 1; leftIterator >= 0; leftIterator--) {
        pushToResult(result, minusDays(temp, leftIterator), leftIterator);
      }

      var rightIterator;
      for (rightIterator = 1; rightIterator < (month.days + 1) - temp.getDate(); rightIterator++) {
        pushToResult(result, addDays(temp, rightIterator), rightIterator);
      }
      return result;
    }

    function processWeek(temp) {
      var result = [];
      var iterate;
      var another;

      for (iterate = temp.getDay(); iterate >= 0; iterate--) {
        pushToResult(result, minusDays(temp, iterate), iterate);
      }

      for (another = 1; another < 7 - temp.getDay(); another++) {
        pushToResult(result, addDays(temp, another), another);
      }

      return result;
    }

    function pushToResult(result, date, iterate) {
      result.push({
        number: date.getDate(),
        name: days[date.getDay()],
        monthName: months[date.getMonth()].name,
        today: iterate === 0
      })
    }

    function addDays(date, addDays) {
      return new Date(date.getTime() + (24*60*60*1000 * addDays));
    }

    function minusDays(date, minusDays) {
      return new Date(date.getTime() - (24*60*60*1000 * minusDays))
    }
  }
]);