'use strict';

function NewsController(
  $state,
  NewsCollection,
  UsersCollection,
  EventsCollection
) {
  this.$state = $state;
  this.NewsCollection = NewsCollection;
  this.UsersCollection = UsersCollection;
  this.EventsCollection = EventsCollection;

  this.SHARED_BY_YOU = 'You';

  this.events = [];
  this.showList = false;
  this.prepareNews();
}

NewsController.prototype.prepareNews = function () {
  var events = this.NewsCollection.getEvents();
  angular.forEach(events, function (event) {
    var username = {
      username: this.findUsername(event.userId)
    };
    var newObj = angular.merge({}, event, username);
    this.events.push(newObj)
  }.bind(this));
};

NewsController.prototype.findUsername = function (userId) {
  var users = this.UsersCollection.getData().filter(function (user) {
    return user.userId === userId;
  });

  if (!users.length) {
    return this.SHARED_BY_YOU;
  }
  return users[0].email;
};

NewsController.prototype.search = function (query) {
  this.query = query;
};

NewsController.prototype.getEvents = function () {
  if (this.query) {
    return this.events.filter(function (event) {
      var lowerTitle = event.title.toLowerCase();
      var lowerUsername = event.username.toLowerCase();
      var lowerQuery = this.query.toLowerCase();
      return lowerTitle.includes(lowerQuery)
        || lowerUsername.includes(lowerQuery);
    }.bind(this));
  }
  return this.events;
};

NewsController.prototype.toggleList = function () {
  this.showList = !this.showList;
};

NewsController.prototype.delete = function (event) {
  this.EventsCollection.delete(event.id)
};

NewsController.prototype.edit = function (event) {
  this.$state.go('events.edit', {id: event.id});
};

NewsController.prototype.isEventUsers = function (event) {
  return event.username === this.SHARED_BY_YOU;
};

NewsController.prototype.showEventDetails = function (event) {
  var readOnly = event.username !== this.SHARED_BY_YOU;
  this.$state.go('events.details', {
    id: event.id,
    readOnly: readOnly
  });
};

NewsController.$inject = [
  '$state',
  'NewsCollection',
  'UsersCollection',
  'EventsCollection'
];

angular.module('main').controller('NewsController', NewsController);