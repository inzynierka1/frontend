'use strict';

angular.module('main').service('UsersCollection', [
  '$http',
  '$q',
  'API',
  function (
    $http,
    $q,
    API
  ) {
    this.usersInfo = [];
    var url = API + 'users';

    this.fetch = function () {
      return $http.get(url + '/names')
        .then(function (response) {
          this.usersInfo = response.data;
          return this.usersInfo;
        }.bind(this));
    };

    this.reset = function () {
      this.usersInfo = [];
    };

    this.getData = function () {
      return this.usersInfo;
    };
  }
]);