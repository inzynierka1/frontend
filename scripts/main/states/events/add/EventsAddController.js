'use strict';

function EventsAddController(
  $state,
  $stateParams,
  EventsCollection,
  GroupsCollection,
  FileUploader,
  CONTENT_API
) {
  this.$state = $state;
  this.EventsCollection = EventsCollection;
  this.CONTENT_API = CONTENT_API;

  this.groups = GroupsCollection.getInfo();

  this.uploader = new FileUploader();
  this.uploader.filters.push({
    name: 'imageFilter',
    fn: function (item, options) {
      var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
      return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }
  });

  this.sharingGroups = [];
  this.images = [];
  this.imgToDelete = [];

  if ($stateParams.id) {
   this.populateForm($stateParams.id);
  } else {
    this.setupDefaultPlaceholders();
  }
}

EventsAddController.prototype.setupDefaultPlaceholders = function () {
  this.header = 'New Event';
  this.namePlaceholder = 'Event Title';
  this.contentPlaceholder = 'Content...';
  this.buttonName = 'Create';
};

EventsAddController.prototype.removeImg = function (img) {
  var removed = this.images.splice(this.images.indexOf(img), 1);
  this.imgToDelete.push(removed[0].id);
};

EventsAddController.prototype.populateForm = function (id) {
  this.event = this.EventsCollection.findById(id);
  console.log(this.event);

  this.header = 'Edit ' + this.event.title;
  this.title = this.event.title;
  this.content = this.event.content;
  this.sharingGroups = this.event.groups;

  this.images = this.event.images.map(function (image) {
    return {
      id: image.id,
      url: this.CONTENT_API + 'images/' + this.event.userId + '/' + this.event.id + '/' + image.id
    }
  }.bind(this));
  this.buttonName = 'Edit';
};

EventsAddController.prototype.getImages = function () {
  return this.images || [];
};

EventsAddController.prototype.shouldShowImgList = function () {
  var toUpload = this.uploader.queue.length;
  var actual = this.images.length;
  return toUpload + actual > 0;
};

EventsAddController.prototype.search = function (query) {
  this.query = query;
};

EventsAddController.prototype.getGroups = function () {
  if (this.query) {
    return this.groups.filter(function (group) {
      var lowerName = group.name.toLowerCase();
      return lowerName.includes(this.query.toLowerCase());
    }.bind(this));
  }
  return this.groups;
};

EventsAddController.prototype.groupAlreadyAdded = function (group) {
  return this.sharingGroups.indexOf(group.id) !== -1;
};

EventsAddController.prototype.addGroup = function (group) {
  this.sharingGroups.push(group.id);
};

EventsAddController.prototype.removeGroup = function (group) {
  this.sharingGroups.splice(this.sharingGroups.indexOf(group.id), 1);
};

EventsAddController.prototype.isValidGroup = function () {
  return true;
};

EventsAddController.prototype.submit = function () {
  if (this.event) {
    this.update();
  } else {
    this.createEvent();
  }
};

EventsAddController.prototype.update = function () {
  var data = {
    title: this.title,
    content: this.content,
    groups: this.sharingGroups,
    images: this.getImagesUpdate()
  };

  data = angular.merge({}, this.event, data);

  this.EventsCollection.update(data, this.event.id, this.uploader);
};

EventsAddController.prototype.getImagesUpdate = function () {
  var images = this.event.images;

  angular.forEach(this.imgToDelete, function (id) {
    var toDelete = images.filter(function (img) {
      return img.id === id;
    });
    images.splice(images.indexOf(toDelete), 1);
  });

  return images;
};

EventsAddController.prototype.createEvent = function () {
  var data = {
    title: this.title,
    content: this.content,
    groups: this.sharingGroups
  };

  this.EventsCollection.save(data, this.uploader);
};

EventsAddController.$inject = [
  '$state',
  '$stateParams',
  'EventsCollection',
  'GroupsCollection',
  'FileUploader',
  'CONTENT_API'
];

angular.module('main').controller('EventsAddController', EventsAddController);