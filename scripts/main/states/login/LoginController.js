'use strict';

function LoginController (
  UserModel
) {
  this.userModel = UserModel;
}

LoginController.prototype.login = function (model) {
  var credentials = {
    login: model.login,
    password: model.password
  };
  console.log(credentials);
  this.userModel.login(credentials);
};

LoginController.prototype.isFormValid = function (model) {
  return model && model.login
    && model.password;
};

LoginController.$inject = [
  'UserModel'
];

angular.module('main').controller('LoginController', LoginController);