'use strict';

angular.module('main').service('groupsAddResolver', [
  'UsersCollection',
  function (
    UsersCollection
  ) {
    this.resolve = function () {
      UsersCollection.reset();

      return UsersCollection.fetch()
        .then(function (response) {
          console.log(response, 'groupsAddResolver');
        });
    }
  }
]);