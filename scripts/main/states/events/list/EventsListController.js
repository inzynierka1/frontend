'use strict';

function EventsListController(
  $state,
  EventsCollection
) {
  this.$state = $state;
  this.EventsCollection = EventsCollection;
  this.showList = false;
}

EventsListController.prototype.getEvents = function () {
  if (this.query) {
    return this.EventsCollection.getEvents()
      .filter(function (event) {
        var lowerTitle = event.title.toLowerCase();
        var lowerQuery = this.query.toLowerCase();
        return lowerTitle.includes(lowerQuery);
      }.bind(this));
  }
  return this.EventsCollection.getEvents();
};

EventsListController.prototype.toggleList = function () {
  this.showList = !this.showList;
};

EventsListController.prototype.showEventDetails = function (event) {
  this.$state.go('events.details', {
    id: event.id,
    readOnly: false
  });
};

EventsListController.prototype.search = function (query) {
  this.query = query;
};

EventsListController.prototype.edit = function (event) {
  this.$state.go('events.edit', {id: event.id});
};

EventsListController.prototype.delete = function (event) {
  this.EventsCollection.delete(event.id);
};

EventsListController.$inject = [
  '$state',
  'EventsCollection'
];

angular.module('main').controller('EventsListController', EventsListController);