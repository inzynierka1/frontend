'use strict';

angular.module('main').service('GroupsCollection', [
  '$http',
  '$q',
  'toaster',
  'API',
  function (
    $http,
    $q,
    toaster,
    API
  ) {
    this.groupsCollection = [];
    var url = API + 'groups';

    this.fetch = function () {
      return $http.get(url)
        .then(function (response) {
          console.log(response.data);
          this.groupsCollection = response.data;
          return $q.when('passing groups via fetch to resolver');
        }.bind(this));
    };

    this.fetchOne = function (id) {
      return $http.get(url + '/' + id)
        .then(function (response) {
          this.groupsCollection.push(response.data);
          return $q.when(response.data);
        }.bind(this));
    };

    this.findById = function (id) {
      return this.groupsCollection
        .find(function (group) {
          return group.id === id;
        });
    };

    this.reset = function () {
      this.groupsCollection = [];
    };

    this.getInfo = function () {
      return this.groupsCollection;
    };

    this.delete = function (groupId) {
      $http.delete(url + '/' + groupId)
        .then(function () {
          toaster.pop('success', '', 'Group was successfully deleted.');
          this.fetch();
        }.bind(this));
    };

    this.save = function (data) {
      return $http.post(url, data)
        .then(function (response) {
          console.log(response);
          toaster.pop('success', '', 'Group was successfully created.');
          return $q.when('success');
        });
    };

    this.update = function (data, id) {
      return $http.put(url + '/' + id, data)
        .then(function (response) {
          console.log(response);
          toaster.pop('success', '', 'Changes were correctly saved.');
          return $q.when('success');
        });
    };

    this.isNotEmpty = function () {
      return this.groupsCollection && this.groupsCollection.length !== 0;
    };

    this.addUser = function (groupId, userId) {
      var data = [
        userId
      ];
      $http.post(url + '/' + groupId, data)
    };
  }
]);