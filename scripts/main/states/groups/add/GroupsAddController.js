function GroupsAddController (
  $state,
  $stateParams,
  GroupsCollection,
  UsersCollection
) {
  this.$state = $state;
  this.GroupsCollection = GroupsCollection;

  this.users = UsersCollection.getData();
  this.sharingUsers = [];

  this.header = 'New Group';
  this.groupNamePlaceholder = 'Group Name';
  this.buttonName = 'Create';

  if ($stateParams.id) {
    this.populateForm($stateParams.id);
  }
}

GroupsAddController.prototype.populateForm = function (id) {
  this.group = this.GroupsCollection.findById(id);
  this.header = 'Edit ' + this.group.name;
  this.name = this.group.name;
  this.groupNamePlaceholder = 'New Group Name';
  this.buttonName = 'Edit';
  this.sharingUsers = this.group.users.map(function (user) {
    return user.userId;
  });
};

GroupsAddController.prototype.addUser = function (user) {
  this.sharingUsers.push(user.userId);
};

GroupsAddController.prototype.search = function (searchQuery) {
  this.searchQuery = searchQuery;
};

GroupsAddController.prototype.userAlreadyAdded = function (user) {
  return this.sharingUsers.indexOf(user.userId) !== -1;
};

GroupsAddController.prototype.removeUser = function (user) {
  this.sharingUsers.splice(this.sharingUsers.indexOf(user.userId), 1);
};

GroupsAddController.prototype.getUsers = function () {
  if (this.searchQuery) {
    return this.users.filter(function (user) {
      var lowerEmail = user.email.toLowerCase();
      return lowerEmail.includes(this.searchQuery.toLowerCase());
    }.bind(this));
  }
  return this.users;
};

GroupsAddController.prototype.submit = function () {
  if (this.group) {
    this.update();
  } else {
    this.createGroup();
  }
};

GroupsAddController.prototype.update = function () {
  var data = {
    name: this.name,
    users: this.sharingUsers
  };

  this.GroupsCollection.update(data, this.group.id)
    .then(function () {
      this.$state.go('groups.list');
    }.bind(this));
};

GroupsAddController.prototype.createGroup = function () {
  var data = {
    name: this.name,
    users: this.sharingUsers
  };

  this.GroupsCollection.save(data)
    .then(function () {
      this.$state.go('groups.list');
    }.bind(this));
};

GroupsAddController.$inject = [
  '$state',
  '$stateParams',
  'GroupsCollection',
  'UsersCollection'
];

angular.module('main').controller('GroupsAddController', GroupsAddController);