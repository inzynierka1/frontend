'use strict';

angular.module('main').service('NewsCollection', [
  '$http',
  '$q',
  'toaster',
  'API',
  function (
    $http,
    $q,
    toaster,
    API
  ) {
    this.events = [];
    var url = API + 'events';

    this.fetch = function () {
      return $http.get(url)
        .then(function (response) {
          this.events = response.data;
          return $q.when('passing news via resolver');
        }.bind(this))
        .catch(function () {
          return $q.reject();
        });
    };

    this.getEvents = function () {
      return this.events;
    };

    this.reset = function () {
      this.events = [];
    };
  }
]);