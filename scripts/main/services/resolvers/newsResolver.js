'use strict';

angular.module('main').service('newsResolver', [
  '$q',
  'NewsCollection',
  'UsersCollection',
  function (
    $q,
    NewsCollection,
    UsersCollection
  ) {
    this.resolve = function () {
      NewsCollection.reset();
      UsersCollection.reset();

      return $q.all([
        NewsCollection.fetch(),
        UsersCollection.fetch()
      ]);
    };
  }
]);