'use strict';

function RegisterController(UserModel) {
  this.userModel = UserModel;
}

RegisterController.prototype.register = function (model) {
  this.userModel.registerUser({
    email: model.email,
    password: model.password
  });
};

RegisterController.prototype.isFormValid = function (user) {
  return this.isUsernameValid(user)
    && this.isPasswordValid(user);
};

RegisterController.prototype.isUsernameValid = function (user) {
  return user && user.email;
};

RegisterController.prototype.isPasswordValid = function (user) {
  return user && user.password && user.password.length > 5
    && user.password === user.passwordConfirm;
};

RegisterController.$inject = [
  'UserModel'
];

angular.module('main').controller('RegisterController', RegisterController);