'use strict';

angular.module('main').service('EventsCollection', [
  '$http',
  '$q',
  '$state',
  'toaster',
  'API',
  'CONTENT_API',
  function (
    $http,
    $q,
    $state,
    toaster,
    API,
    CONTENT_API
  ) {
    this.userEvents = [];
    var url = API + 'events';
    var imagesUrl = CONTENT_API + 'images';

    this.fetch = function () {
      return $http.get(url + '/self')
        .then(function (response) {
          this.userEvents = response.data;
          return $q.when('passing events via resolver');
        }.bind(this))
        .catch(function () {
          return $q.reject();
        });
    };

    this.fetchOne = function (id) {
      return $http.get(url + '/' + id)
        .then(function (response) {
          this.userEvents.push(response.data);
          return $q.when(response.data);
        }.bind(this))
        .catch(function () {
          return $q.reject();
        });
    };

    this.findById = function (id) {
      return this.userEvents
        .find(function (event) {
          return event.id === id;
        });
    };

    this.save = function (data, uploader) {
      return $http.post(url, data)
        .then(function (response) {
          if (!uploader.queue.length) {
            toaster.pop('success', '', 'Event was successfully created.');
            $state.go('events.list');
            return $q.when('success');
          }
          return this.uploadImg(response.data, uploader, 'Event was successfully created.');
        }.bind(this));
    };

    this.uploadImg = function (responseData, uploader, msg) {
      var url = imagesUrl + '/' + responseData.userId + '/' + responseData.id;
      uploader.url = url;
      angular.forEach(uploader.queue, function (item) {
        item.url = url;
      });
      uploader.uploadAll();

      uploader.onCompleteAll = function () {
        toaster.pop('success', '', msg);
        $state.go('events.list');

      };
      return $q.when();
    };

    this.delete = function (eventId) {
      $http.delete(url + '/' + eventId)
        .then(function () {
          toaster.pop('success', '', 'Event was successfully deleted.');
          this.fetch();
        }.bind(this));
    };

    this.update = function (data, eventId, uploader) {
      return $http.put(url + '/' + eventId, data)
        .then(function () {
          if (!uploader.queue.length) {
            toaster.pop('success', '', 'Changes were correctly saved.');
            $state.go('events.list');
            return $q.when('success');
          }
          return this.uploadImg(data, uploader, 'Changes were correctly saved.');
        }.bind(this));
    };

    this.getEvents = function () {
      return this.userEvents;
    };

    this.reset = function () {
      this.userEvents = [];
    };
  }
]);
