'use strict';

angular.module('main').service('UserModel', [
  '$http',
  '$state',
  '$q',
  'toaster',
  'tokenStorage',
  'API',
  function (
    $http,
    $state,
    $q,
    toaster,
    tokenStorage,
    API
  ) {
    this.currentUser = {};

    this.registerUser = function (model) {
      return $http.post(API + 'register', model)
        .then(function (response) {
          toaster.pop('success', "", "Successfully registered.");
          tokenStorage.store(response.data.token);
          $state.go('news');
        })
        .catch(function (response) {
          console.log(response);
          toaster.pop('warning', "", "Cannot register new account. Please try again later.");
        });
    };

    this.login = function (model) {
      var coded = btoa(model.login + ':' + model.password);

      return $http.get(
        API + 'login',
        {headers: {Authorization: coded}}
      )
        .then(function (response) {
          toaster.pop('success', "", "Successfully logged in.");
          tokenStorage.store(response.data.token);
          $state.go('news');
        })
        .catch(function (response) {
          console.log(response);
          toaster.pop('warning', "", "Cannot log in. Please try again later.");
        });
    };

    this.fetch = function () {
      $http.get(
        API + 'self'
      )
        .then(function (response) {
          console.log(response.data);
          this.currentUser = response.data
        }.bind(this))
        .catch(function (response) {
          console.log(response);
        });
      return $q.when('passing via fetch to resolver');
    };

    this.reset = function () {
      console.log('reset');
    };

    this.logout = function () {
      tokenStorage.clear();
    };

    this.isLogged = function () {
     return tokenStorage.isAuthenticated();
    };

    this.getInfo = function () {
      return this.currentUser;
    };
  }
]);