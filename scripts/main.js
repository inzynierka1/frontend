'use strict';

angular.module('main', [
  'ngAnimate',
  'toaster',
  'ui.router',
  'ui.bootstrap',
  'ngMaterial',
  'angularFileUpload'
]);

angular.module('main').config([
  '$httpProvider',
  '$stateProvider',
  '$urlRouterProvider',
  function (
    $httpProvider,
    $stateProvider,
    $urlRouterProvider
  ) {
    $httpProvider.interceptors.push('tokenAuthInterceptor');
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('home', {
        url: '/',
        template: ''
      })
      .state('login', {
        url: '/login',
        templateUrl: 'scripts/main/states/login/login.html',
        controller: 'LoginController',
        controllerAs: 'loginCtrl'
      })
      .state('register', {
        url: '/register',
        templateUrl: 'scripts/main/states/register/register.html',
        controller: 'RegisterController',
        controllerAs: 'registerCtrl'
      })
      .state('news', {
        url: '/news',
        templateUrl: 'scripts/main/states/news/news.html',
        controller: 'NewsController',
        controllerAs: 'newsCtrl',
        resolve: { dependencies : ['newsResolver',
          function (newsResolver) {
            return newsResolver.resolve();
          }]}
      })
      .state('userPanel', {
        url: '/userPanel',
        templateUrl: 'scripts/main/states/userPanel/userPanel.html',
        controller: 'UserPanelController',
        controllerAs: 'userPanelCtrl',
        resolve: { dependencies: ['userPanelResolver',
          function (userPanelResolver) {
            return userPanelResolver.resolve();
          }]}
      })
      .state('events', {
        url: '/events',
        template: '<ui-view></ui-view>',
        defaultChildState: '.list'
      })
      .state('events.list', {
        url: '/list',
        templateUrl: 'scripts/main/states/events/list/eventsList.html',
        controller: 'EventsListController',
        controllerAs: 'eventsListCtrl',
        resolve: { dependencies: ['eventsResolver',
          function (eventsResolver) {
            return eventsResolver.resolve();
          }]}
      })
      .state('events.add', {
        url: '/add',
        templateUrl: 'scripts/main/states/events/add/eventsAdd.html',
        controller: 'EventsAddController',
        controllerAs: 'eventsAddCtrl',
        resolve: { dependencies: ['groupsResolver',
          function (groupsResolver) {
            return groupsResolver.resolve();
          }]}
      })
      .state('events.details', {
        url: '/:id/:readOnly',
        templateUrl: 'scripts/main/states/events/details/eventDetails.html',
        controller: 'EventDetailsController',
        controllerAs: 'eventDetailsCtrl',
        resolve: { dependencies: ['eventDetailsResolver', '$stateParams',
          function (eventDetailsResolver, $stateParams) {
            return eventDetailsResolver.resolve($stateParams.id);
          }]}
      })
      .state('events.edit', {
        url: '/edit/:id',
        templateUrl: 'scripts/main/states/events/add/eventsAdd.html',
        controller: 'EventsAddController',
        controllerAs: 'eventsAddCtrl',
        resolve: { dependencies: [
          '$q',
          '$stateParams',
          'groupsResolver',
          'eventDetailsResolver',
          function ($q, $stateParams, groupsResolver, eventDetailsResolver) {
            return $q.all([
              groupsResolver.resolve(),
              eventDetailsResolver.resolve($stateParams.id)
            ]);
          }
        ]}
      })
      .state('groups', {
        url: '/groups',
        template: '<ui-view></ui-view>',
        defaultChildState: '.list'
      })
      .state('groups.list', {
        url: '/list',
        templateUrl: 'scripts/main/states/groups/groups.html',
        controller: 'GroupsController',
        controllerAs: 'groupsCtrl',
        resolve: { dependencies: ['groupsResolver',
          function (groupsResolver) {
            return groupsResolver.resolve();
          }]}
      })
      .state('groups.add', {
        url: '/add',
        templateUrl: 'scripts/main/states/groups/add/groupsAdd.html',
        controller: 'GroupsAddController',
        controllerAs: 'groupsAddCtrl',
        resolve: {dependencies: ['groupsAddResolver',
          function (groupsAddResolver) {
            return groupsAddResolver.resolve();
          }]}
      })
      .state('groups.details', {
        url: '/:id',
        templateUrl: 'scripts/main/states/groups/details/groupDetails.html',
        controller: 'GroupDetailsController',
        controllerAs: 'groupDetailsCtrl',
        resolve: {dependencies: ['groupDetailsResolver', '$stateParams',
          function (groupDetailsResolver, $stateParams) {
            return groupDetailsResolver.resolve($stateParams.id);
          }]}
      })
      .state('groups.edit', {
        url: '/edit/:id',
        templateUrl: 'scripts/main/states/groups/add/groupsAdd.html',
        controller: 'GroupsAddController',
        controllerAs: 'groupsAddCtrl',
        resolve: {
          dependencies: [
            '$q',
            'groupsAddResolver',
            'groupDetailsResolver',
            '$stateParams',
            function ($q, groupsAddResolver, groupDetailsResolver, $stateParams) {
              return $q.all([
                groupsAddResolver.resolve(),
                groupDetailsResolver.resolve($stateParams.id)
              ]);
          }]}
      });
}]);