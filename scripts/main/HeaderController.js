'use strict';

function HeaderController(
  $state,
  UserModel
) {
  this.$state = $state;
  this.userModel = UserModel;
}

HeaderController.prototype.logout = function () {
  this.userModel.logout();
  this.$state.go('home');
};

HeaderController.$inject = [
  '$state',
  'UserModel'
];

angular.module('main').controller('HeaderController', HeaderController);