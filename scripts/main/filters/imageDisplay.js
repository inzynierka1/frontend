'use strict';

angular.module('main').filter('imageDisplay', [
  'CONTENT_API',
  function (
    CONTENT_API
  ) {
    return function (event) {
      if (event && event.images && event.images.length > 0) {
        var image = event.images[0];
        return CONTENT_API + 'images/' + event.userId + '/' + event.id + '/' + image.id;
      }
      return 'images/placeholderImg.png';
    }
  }
]);