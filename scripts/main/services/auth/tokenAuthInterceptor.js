'use strict';

angular.module('main').service('tokenAuthInterceptor', [
  '$q',
  '$injector',
  'tokenStorage',
  function (
    $q,
    $injector,
    tokenStorage
  ) {
    return {
      request: function (config) {
        var authToken = tokenStorage.retrieve();
        if (authToken) {
          config.headers['X-AUTH-TOKEN'] = authToken;
        }
        return config;
      },
      responseError: function (error) {
        if (error.status === 401 || error.status === 403) {
          tokenStorage.clear();
          var state = $injector.get('$state');
          state.go('login');
        }
        return $q.reject(error);
      }
    };
  }
]);