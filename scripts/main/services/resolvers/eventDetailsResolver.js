'use strict';

angular.module('main').service('eventDetailsResolver', [
  'EventsCollection',
  function (
    EventsCollection
  ) {
    this.resolve = function (id) {
      return EventsCollection.fetchOne(id);
    };
  }
]);