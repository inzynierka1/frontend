'use strict';

function EventDetailsController(
  $state,
  $stateParams,
  dependencies,
  CONTENT_API
) {
  this.$state = $state;
  this.$stateParams = $stateParams;
  this.eventModel = dependencies;

  if (this.eventModel.images.length) {
    this.slides = this.eventModel.images.map(function (image) {
      return {
        id: image.id,
        image: CONTENT_API + 'images/' + this.eventModel.userId + '/' + this.eventModel.id + '/' + image.id
      }
    }.bind(this));
    this.active = 0;
  }
}

EventDetailsController.prototype.edit = function () {
  this.$state.go('events.edit', {id: this.eventModel.id});
};

EventDetailsController.prototype.isReadOnly = function () {
  return this.$stateParams.readOnly === 'true';
};

EventDetailsController.$inject = [
  '$state',
  '$stateParams',
  'dependencies',
  'CONTENT_API'
];

angular.module('main').controller('EventDetailsController', EventDetailsController);