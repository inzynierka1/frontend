'use strict';

angular.module('main').filter('dateDisplay', [
  'javaDateUtils',
  function (
    javaDateUtils
  ) {
    function formatDay(date) {
      var day = date.getDate();
      var month = 1 + date.getMonth();
      var year = date.getFullYear();
      return day + '.' + month + '.' + year;
    }

    function formatTime(date) {
      var hours = date.getHours();
      var minutes = date.getMinutes();
      if (minutes < 10) {
        minutes = '0' + minutes;
      }
      return hours + ':' + minutes;
    }

    return function (input) {
      var date = javaDateUtils.getDateFromJavaObject(input);
      return formatTime(date) + ' ' + formatDay(date);
    };
  }
]);
