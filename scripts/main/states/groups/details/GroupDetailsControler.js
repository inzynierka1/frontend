'use strict';

function GroupDetailsController(
  $state,
  $stateParams,
  javaDateUtils,
  GroupsCollection
) {
  this.$state = $state;
  this.$stateParams = $stateParams;
  this.javaDateUtils = javaDateUtils;
  this.GroupsCollection = GroupsCollection;
  this.groupModel = GroupsCollection.findById($stateParams.id);
}

GroupDetailsController.prototype.getDisplayDate = function (date) {
  if (date) {
    return this.javaDateUtils.getDisplayDateFromJavaObject(date);
  }
};

GroupDetailsController.prototype.edit = function () {
  this.$state.go('groups.edit', {id: this.groupModel.id});
};

GroupDetailsController.$inject = [
  '$state',
  '$stateParams',
  'javaDateUtils',
  'GroupsCollection'
];

angular.module('main').controller('GroupDetailsController', GroupDetailsController);