'use strict';

function UserPanelController(
  $http,
  UserModel
) {
  this.msg = 'UserPanelCtrl - works!';
  this.userModel = UserModel;
  this.userInfo = this.userModel.getInfo();
  this.$http = $http;
  console.log(this.userInfo);
}

UserPanelController.prototype.test = function () {
  this.$http.get(
    'http://192.168.0.143:8081/api/test'
  ).then(function (res) {
    console.log(res);
  }).catch(function (res) {
    console.log(res);
  })
};

UserPanelController.prototype.upload = function () {
  console.log(this.file);
  var fd = new FormData();
  fd.append('file', this.file);
  this.$http.post(
    'http://192.168.0.143:8081/api/images',
    fd,
    {
      transformRequest: angular.identity,
      headers: {'Content-Type': undefined}
    })
    .then(function (res) {
      var id = res.data.id;
      var userId = res.data.userId;

      console.log(res);
    })

    .catch(function (res) {
      console.log(res);
    });
};

UserPanelController.$inject = [
  '$http',
  'UserModel'
];

angular.module('main').controller('UserPanelController', UserPanelController);