'use strict';

angular.module('main').service('groupsResolver', [
  'GroupsCollection',
  function (
    GroupsCollection
  ) {
    this.resolve = function () {
      GroupsCollection.reset();

      return GroupsCollection.fetch()
        .then(function (response) {
          console.log(response);
        });
    }
  }
]);