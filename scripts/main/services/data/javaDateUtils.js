'use strict';

//Months in JS Date 0-11

angular.module('main').service('javaDateUtils', [
  function () {
    this.getDisplayDateFromJavaObject = function (javaDate) {
      var options = {
        weekday: "long", year: "numeric", month: "short",
        day: "numeric", hour: "2-digit", minute: "2-digit"
      };
      var date = this.getDateFromJavaObject(javaDate);
      return date.toLocaleTimeString("en-us", options);
    };

    this.getDateFromJavaObject = function (javaDate) {
      if (javaDate) {
        var tokens = javaDate.split('T');
        var calendarDate = this.mapCalendarDate(tokens[0].split('-'));
        var hours = this.mapHours(tokens[1].split(':'));

        return new Date(calendarDate.year,
          calendarDate.month,
          calendarDate.day,
          hours.hour,
          hours.minutes,
          hours.seconds,
          hours.milliseconds);
      }
    };

    this.mapCalendarDate = function (tokens) {
      return {
        year: Number(tokens[0]),
        month: Number(tokens[1]) - 1,
        day: Number(tokens[2])
      };
    };

    this.mapHours = function (tokens) {
      var secondsAndMillis = tokens[2].split('.');
      return {
        hour: Number(tokens[0]),
        minutes: Number(tokens[1]),
        seconds: Number(secondsAndMillis[0]),
        milliseconds: Number(secondsAndMillis[1])
      };
    };
  }
]);