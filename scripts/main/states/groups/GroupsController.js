'use strict';

function GroupsController(
  $state,
  GroupsCollection
) {
  this.$state = $state;
  this.GroupsCollection = GroupsCollection;
}

GroupsController.prototype.getCollection = function () {
  if (this.query) {
    return this.GroupsCollection.getInfo()
      .filter(function (group) {
        var lowerTitle = group.name.toLowerCase();
        var lowerQuery = this.query.toLowerCase();
        return lowerTitle.includes(lowerQuery);
      }.bind(this));
  }
  return this.GroupsCollection.getInfo();
};

GroupsController.prototype.getUserCount = function (users) {
  return users ? users.length : 0;
};

GroupsController.prototype.hasGroups = function () {
  return this.GroupsCollection.isNotEmpty();
};

GroupsController.prototype.search = function (query) {
  this.query = query;
};

GroupsController.prototype.edit = function (group) {
  this.$state.go('groups.edit', {id: group.id});
};

GroupsController.prototype.delete = function (group) {
  this.GroupsCollection.delete(group.id);
};

GroupsController.prototype.showGroupDetails = function (group) {
  this.$state.go('groups.details', {id: group.id});
};

GroupsController.$inject = [
  '$state',
  'GroupsCollection'
];

angular.module('main').controller('GroupsController', GroupsController);