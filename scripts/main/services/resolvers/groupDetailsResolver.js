'use strict';

angular.module('main').service('groupDetailsResolver', [
  'GroupsCollection',
  function (
    GroupsCollection
  ) {
    this.resolve = function (id) {
      var group = GroupsCollection.findById(id);
      if (group) {
        return group;
      } else {
        return GroupsCollection.fetchOne(id);
      }
    }
  }
]);