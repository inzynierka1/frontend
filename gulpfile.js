var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task('main-scripts', function() {
  return gulp.src('scripts/main/**/*.js')
    .pipe(concat('main.src.js'))
    .pipe(gulp.dest('scripts/'))
});

gulp.task('main-watch', function() {
  gulp.watch('scripts/main/**/*.js', ['main-scripts']);
});

gulp.task('main',  ['main-scripts', 'main-watch']);

gulp.task('default', ['main']);