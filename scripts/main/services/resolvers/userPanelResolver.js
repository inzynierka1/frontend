'use strict';

angular.module('main').service('userPanelResolver', [
  'UserModel',
  function (
    UserModel
  ) {
    this.resolve = function () {
      UserModel.reset();

      return UserModel.fetch()
        .then(function (response) {
          console.log(response);
        });
    }
  }
]);